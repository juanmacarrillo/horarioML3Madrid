#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 26 13:21:46 2019

@author: j
"""

import requests
import datetime
import time
from telegram.ext import Updater, MessageHandler, Filters
import telegram
import os

path = os.path.dirname(__file__)
pathToToken = os.path.join(path, "token.txt")
tokenFile = open(pathToToken, "r")
token = tokenFile.read()
token = token.rstrip()

stationDict = {"Puerta de Boadilla" : "362",
               "Infante D. Luis" : "361",
               "Siglo XXI" : "360",
               "Nuevo Mundo" : "359",
               "Boadilla Centro" : "358",
               "Ferial de Boadilla" : "357",
               "Cantabria" : "356",
               "Prado del Espino" : "355",
               "Ventorro del Cano" : "354",
               "Montepríncipe" : "353",
               "Retamares" : "352",
               "Cocheras" : "351",
               "Ciudad del Cine" : "350",
               "José Isbert" : "349",
               "Ciudad de la Imagen" : "348",
               "Colonia Jardín" : "201"
               }


def searchInDict(dictionary, searchFor):
    for k in dictionary:
        if searchFor.lower() in k.lower():
            return dictionary[k]


def metroLigero(estacionOrigen, estacionDestino, dictionary):  
    
    reversedDict = dict(zip(stationDict.values(),stationDict.keys()))
                   
    origen = searchInDict(dictionary, estacionOrigen)
    destino = searchInDict(dictionary, estacionDestino)
    
    originalOrigen = searchInDict(reversedDict, origen)
    originalDestino = searchInDict(reversedDict, destino)
    
    url = "https://www.metroligero-oeste.es/api/next-stop?origin=" + origen + "&destination=" + destino
    
    response = requests.get(url)
    
    request_time = response.json()["data"]["date"]
    request_time = request_time[:-6]
    
    tm_object = time.strptime(request_time, '%a, %d %b %Y %X')
    datetime_object = datetime.datetime(*tm_object[:6])

    time_left_1 = response.json()["data"]["first_stop"]
    minutes_left_1 = str(time_left_1 // 60) + ":" + str(time_left_1 % 60 * 60 // 10)
    when_1 = datetime_object + datetime.timedelta(seconds=time_left_1)
    when_1 = when_1.strftime("%X")

    time_left_2 = response.json()["data"]["second_stop"]
    minutes_left_2 = str(time_left_2 // 60) + ":" + str(time_left_2 % 60 * 60 // 10)
    when_2 = datetime_object + datetime.timedelta(seconds=time_left_2)
    when_2 = when_2.strftime("%X")

    mensaje = "_Metro de %s a %s_: \
    Siguiente a las `%s` (en *%s min*)\
    Próximo a las `%s` (en *%s minutos*)"\
    % (originalOrigen, originalDestino, when_1, minutes_left_1, when_2, minutes_left_2)

    return(mensaje)
    
# metroLigero("cantabria", "colonia", stationDict)

    
updater = Updater(token=token, use_context=True)
dispatcher = updater.dispatcher 

   
def sendMensaje(update, context):
    inputFromUser = update.message.text.split(" ")
    mensaje = metroLigero(inputFromUser[0], inputFromUser[1], dictionary = stationDict)
    context.bot.send_message(chat_id=update.effective_chat.id,
                             parse_mode=telegram.ParseMode.MARKDOWN,
                             text=mensaje)

ml_handler = MessageHandler(Filters.text, sendMensaje)
dispatcher.add_handler(ml_handler)


updater.start_polling()

#def echo(update, context):
#    context.bot.send_message(chat_id=update.effective_chat.id, text=update.message.text)
#echo_handler = MessageHandler(Filters.text, echo)
#dispatcher.add_handler(echo_handler)
#updater.idle()
#updater.stop()
